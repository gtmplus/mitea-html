//= ../../node_modules/jquery/dist/jquery.slim.js
//= ../../node_modules/bootstrap/dist/js/bootstrap.min.js
//= ../../node_modules/swiper/swiper-bundle.js

window.addEventListener('load', () => {

  // safe init func for sliders
  function safeInit(selector, conf) {
    if (selector) {
      new Swiper(selector, conf);
    }else{
      console.log('Undefined selector, or wrong config')
    };
  };

  // modals
  const modalTriggers = document.querySelectorAll('[data-modal-trigger]');
  const modals = document.querySelectorAll('[data-modal]');
  const modalClose = document.querySelectorAll('[data-close]');
  const body = document.querySelector('body.page-wrapper');
  let currentModal;

  modalTriggers.forEach(element => {
    element.addEventListener('click', (e) => {
      e.preventDefault();
      modals.forEach(elem => {
        if (element.dataset.modalTrigger === elem.dataset.modal) {
          elem.classList.add('show');
          body.classList.add('no-scroll');
          currentModal = elem;
        }
      });
    });
  });

  modalClose.forEach(close => {
    close.addEventListener('click', () => {
      currentModal.classList.remove('show');
      body.classList.remove('no-scroll');
    });
  });

  // intro slider
  const introSlider = document.querySelector('.intro-slider');
  const introSliderConf = {
    centeredSlides: true,
    slidesPerView: 1,
    loop: true,
    observer: true,
    observeParents: true,
    observeSlideChildren: true,
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-dots',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  };

  safeInit(introSlider, introSliderConf);

  // features section tab-sliders init
  const featuresSliders = document.querySelectorAll('.tab-slider');
  const navPill = document.querySelectorAll('.nav .nav-category');
  const featuresSlidersConf = {
    centeredSlides: false,
    slidesPerView: 3,
    observer: true,
    observeParents: true,
    observeSlideChildren: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    // Responsive breakpoints
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      // when window width is >= 640px
      640: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      // when window width is >= 768px
      768: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      // when window width is >= 992px
      992: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    }
  }

  const sliders = [
    '.tab-slider-gartenparty',
    '.tab-slider-firmenfeier',
    '.tab-slider-hochzeit',
    '.tab-slider-messe',
    '.tab-slider-weihnachtsfeier'
  ];

  featuresSliders.forEach(slider => {
    if (slider.parentNode.classList.contains("active")) {
      sliders.forEach(slider => (safeInit(slider, featuresSlidersConf)));
    }
  });

  navPill.forEach((pill) => {
    pill.addEventListener("click", () => {
      sliders.forEach(slider => {
        if (slider.parentNode.classList.contains("active")) {
          setTimeout(() => {
            safeInit(slider, featuresSlidersConf);
          }, 200)
        }
      });
    })
  })


  // section search-form
  const keywords = document.querySelectorAll('.search-keyword');
  const search = document.querySelector('.searchbar');

  keywords.forEach((element) => {
    element.addEventListener("click", () => {
      search.value = element.textContent;
    });
  });

  // inspiration section slider
  const inspSlider = document.querySelector('.inspiration-slider');
  const inspSliderConf = {
    centeredSlides: false,
    slidesPerView: 3,
    loop: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    // Responsive breakpoints
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      // when window width is >= 640px
      640: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      // when window width is >= 768px
      768: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      // when window width is >= 992px
      992: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    }
  }

  safeInit(inspSlider, inspSliderConf);

  // related products section slider init
  const productsSlider = document.querySelector('.products-slider');
  const productsSliderConf = {
    centeredSlides: false,
    slidesPerView: 4,
    loop: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    // Responsive breakpoints
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      // when window width is >= 640px
      640: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      // when window width is >= 768px
      768: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      // when window width is >= 992px
      992: {
        slidesPerView: 4,
        spaceBetween: 20
      }
    }
  };

  safeInit(productsSlider, productsSliderConf);

  // single product gallery slider init
  var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 20,
    slidesPerView: 5,
    observer: true,
    observeParents: true,
    observeSlideChildren: true,
    loopedSlides: 5, //looped slides should be the same
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 20,
    observer: true,
    observeParents: true,
    observeSlideChildren: true,
    loopedSlides: 5, //looped slides should be the same
    thumbs: {
      swiper: galleryThumbs,
    },
  });

  // sidebar listgroup widget
  const productCategories = document.querySelectorAll('.widget-list-item');

  productCategories.forEach(category => {
    let dropToggler = category.querySelector('.drop-toggler');
    let dropList = category.querySelector('.children');
    dropToggler.addEventListener('click', () => {
      category.classList.toggle("active");
    });
  });

  // filter keywords
  const filtersArea = document.querySelector('.active-filters');

  keywords.forEach(filter => {
    filter.addEventListener("click", () => {
      if(filter.classList.contains('is-active')){return};
      let newFilter = document.createElement('span');
      newFilter.className='active-filters-keyword';
      newFilter.textContent = filter.textContent;
      if(filtersArea){filtersArea.appendChild(newFilter)};
      filter.classList.toggle('is-active');
      newFilter.addEventListener('click', () => {
        newFilter.remove();
        filter.classList.remove('is-active');
      })
    });
  });

  // view list toggler
  const gridBtn = document.querySelector('.grid-view');
  const listBtn = document.querySelector('.list-view');
  const columns = document.querySelectorAll('.col-md-4');
  const productCards = document.querySelectorAll('.product-card');

  if(gridBtn){
    gridBtn.addEventListener('click', () => {
      gridBtn.classList.add('active');
      listBtn.classList.remove('active');
      columns.forEach(column => column.classList.remove('col-md-12'));
      columns.forEach(column => column.classList.add('col-md-4'));
      productCards.forEach(card => card.classList.remove('horizontal'));
    })
  }

  if(listBtn){
    listBtn.addEventListener('click', () => {
      listBtn.classList.add('active');
      gridBtn.classList.remove('active');
      columns.forEach(column => column.classList.remove('col-md-4'));
      columns.forEach(column => column.classList.add('col-md-12'));
      productCards.forEach(card => card.classList.add('horizontal'));
    })
  }
});